Implementation of the Library application in Windows Presentation Foundation with the following functionality:
1)Adding a book to the list of books.
2)Removing selected book from the list.
3)Editing selected book.
4)Filtering a book via its title,author or year.