﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.ObjectModel;
using CBlibrary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CBlibrary
{
    /// <summary>
    /// Interaction logic for AddWindow.xaml
    /// </summary>

    
        public partial class AddWindow : Window
        {

        MainWindow localob;
        public ObservableCollection<Book> local;
        bool localfiltermode;
        int Id = 0;
        public AddWindow(ObservableCollection<Book> b,int lastID,bool filtermode,MainWindow ob)
        {
            InitializeComponent();
            this.local = b;
            this.Id = lastID;
            this.localfiltermode = filtermode;
            this.localob = ob;
        }

       
        public void OnAddButtonClicked(object sender, EventArgs e)
            {
             //if no data is provided
            if(AddAuthorText.Text.Length==0 && AddTitleText.Text.Length == 0 && AddYearText.Text.Length == 0)
            {
                MessageBox.Show("Please Provide Data");
            }
            else
            { //adding book to the collection
            local.Add(new Book(this.Id,AddAuthorText.Text, AddTitleText.Text, AddYearText.Text));

                string line = AddAuthorText.Text + "|" + AddTitleText.Text + "|" + AddYearText.Text; //saving book to the file

                using (StreamWriter w = File.AppendText(@"D:\Documents\education process\EGUI\LabsMine\lab2\WPFLibrary\CBlibrary\library.txt"))
                {
                    w.WriteLine(line);
                }
                //Also write to the file;

                if(localfiltermode)
                {

                    localob.myFilter();
                }
                this.Close();
            }
        }
        }
}
