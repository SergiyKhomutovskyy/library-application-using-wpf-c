﻿using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CBlibrary
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        public EditWindow()
        {
            InitializeComponent();
        }

        public int LocalIndex;
        public ObservableCollection<Book> localbooks;
        public ObservableCollection<Book> localfiltered;
        // string AuthorText;
        // string TitleText;
        // string YearText;
        int Id;
        public EditWindow(int index,ObservableCollection<Book> books, ObservableCollection<Book> filtered,int ID,string AuthortoPut, string TitletoPut,string YearToPut)
        {
            InitializeComponent();

            this.LocalIndex = index;
            this.localbooks = books;
            this.localfiltered = filtered;
            this.Id = ID;

            //this.AuthorText = AuthortoPut1;
            //this.TitleText = AuthortoPut2;
            //this.YearText = AuthortoPut3;

            AuthorEdit.Text = AuthortoPut; //puttin text to EDIT DIALOG textboxes
            TitleEdit.Text = TitletoPut;
            YearEdit.Text = YearToPut;

            ShowDialog();
        }

    
        

        public void OnApplyButtonClicked(object sender, EventArgs e)
        {
            //Book ThatBook = localbooks[LocalIndex];
            //Debug.WriteLine(ThatBook.ID);
            
            Debug.WriteLine(LocalIndex);
            Debug.WriteLine(this.Id);
            int IdtoPut = this.Id;
            int index = this.Id-1;
            Book BookToInsert = new Book(IdtoPut, AuthorEdit.Text, TitleEdit.Text, YearEdit.Text);
            //this.Id--;//->substruct 1 to obtain index in listview
            localbooks.RemoveAt(index);
            localbooks.Insert(index, BookToInsert);

            int FilteredIndex = -1;

            foreach (Book book in localfiltered)
            {
                ++FilteredIndex;
            }
            if (FilteredIndex >= 0)
            {
                localfiltered.RemoveAt(index);
                localfiltered.Insert(index, BookToInsert);
            }

            //storing to file
            FileInfo fi = new FileInfo(@"D:\Documents\education process\EGUI\LabsMine\lab2\WPFLibrary\CBlibrary\library.txt");
            using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
            {
                foreach (Book book in localbooks)
                {
                    string LineToWrite = book.Author + "|" + book.Title + "|" + book.Year + "\n";
                    txtWriter.Write(LineToWrite);

                }
            }

            Close();

        }
    }
}

