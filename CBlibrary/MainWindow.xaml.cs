﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.ObjectModel;
using CBlibrary;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CBlibrary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public class Book
    {
        public int ID { get; set; }

        public string Author { get; set; }
        public string Title { get; set; }
        public string Year { get; set; }

        public Book()
        {

        }
        public Book(string author, string title, string year)
        {
            Author = author;
            Title = title;
            Year = year;
        }

        public Book(int ID,string author, string title, string year)
        {
            this.ID = ID;
            Author = author;
            Title = title;
            Year = year;
        }
    }


    public partial class MainWindow : Window
    {
        public ObservableCollection<Book> books = new ObservableCollection<Book>();
        ObservableCollection<Book> filtered = new ObservableCollection<Book>();
        bool FilterMode = false;


        public int LastID = 0;

        public MainWindow()
        {
            InitializeComponent();
            readFile();

            YearsPool.Items.Add(string.Empty);

            List<int> intList = new List<int>();
            //routine to sort years andd add the to Combobox
            foreach (Book book in books)
            {
                if(intList.Contains(Int32.Parse(book.Year)))
                {
                    continue;
                }
                int x = Int32.Parse(book.Year);
                intList.Add(x);
            }
            int[] arr = intList.ToArray();
            Array.Sort(arr);
            foreach(int ob in arr)
            {
                string year = ob.ToString();
                YearsPool.Items.Add(year);
            }


            ListOfBooks.ItemsSource = books;

            
        }
       

        public void readFile()
        {
            StreamReader file = new StreamReader(@"D:\Documents\education process\EGUI\LabsMine\lab2\WPFLibrary\CBlibrary\library.txt");
            string currentline;
            List<string> myList = new List<string>();
            //string[] words;

            int id = 1;
            while ((currentline = file.ReadLine()) != null)
            {
                string[] words = new string[3];
                words = currentline.Split('|');
                Book book = new Book(id, words[0], words[1], words[2]);
                books.Add(book);
                id++;

            }

            LastID = id;

        }


    //..............................ADDING...............................

        public void AddBookClicked(object sender, EventArgs e)

        {
            var AddDialog = new AddWindow(books,LastID,FilterMode,this);
            AddDialog.ShowDialog();
            LastID++;

        }
     //...................................DELETION..................................................................

        public void DeleteBookClicked(object sender, EventArgs e)
        {
           int i = ListOfBooks.SelectedItems.Count;

            if(i> 1)
            {
                //MessageBox.Show("Please Select One Item!");
               
                System.Collections.IList items = (System.Collections.IList)ListOfBooks.SelectedItems;
                var collection = items.Cast<Book>();
                List<Book> MyList = new List<Book>();

               
                   foreach (var tdl in collection)
                   {
                        MyList.Add(tdl);
                   }
                    
                   foreach(Book book in MyList)
                   {
                    books.Remove(book);
                   }
                
            }

            else
            {
                if (i != 0)
                {
                    Book ToDelete =  (Book)ListOfBooks.SelectedItems[0];
                    books.Remove(ToDelete);
                    filtered.Remove(ToDelete);

                   // books.RemoveAt(ListOfBooks.SelectedIndex);
                    // OnItemWasDeleted += OverwriteFile;
                    overWriteFile();
                   // OnItemWasDeleted();
                }

            }

        }

        public void overWriteFile()
        {
            FileInfo fi = new FileInfo(@"D:\Documents\education process\EGUI\LabsMine\lab2\WPFLibrary\CBlibrary\library.txt");
            using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
            {
                foreach (Book book in books)
                {
                    string LineToWrite = book.Author + "|" + book.Title + "|" + book.Year + "\n";
                    txtWriter.Write(LineToWrite);

                }
            }
        }

        //....................................................EDIT.....................................................

       public void EditBookClicked (object sender, EventArgs e)
        {
            int i = ListOfBooks.SelectedItems.Count;

            if (i > 1)
            {
                MessageBox.Show("Please Select One Item!");
            }

            else
            {
                if (i != 0)
                {
                    
                    int ind = ListOfBooks.SelectedIndex; //storing index of selected book
                    Book RequiredBook = (Book)(ListOfBooks.SelectedItems[0]);//storing selected book
                    int ID = 1;

                    Book Mybook = new Book();
                    foreach ( Book book in books)
                    {
                        if (book.ID == RequiredBook.ID)  //if i found selected book in the main collection
                        {
                            ID = book.ID;  //i take its ID
                            //Debug.WriteLine(ID);
                            Mybook = book;
                            break;
                        }
                    }


                    Debug.WriteLine(ID);
                   
                   
                                                                  //Passing this to put it in the textBoxes
                    var EditDialog = new EditWindow(ind,books,filtered,ID,Mybook.Author,Mybook.Title,Mybook.Year);
                  
                    
                   
                   
                }

            }

        }

        //.................................FILTERING.........................................

        public void FilterButtonClicked(object sender, EventArgs e)
        {
            myFilter();  

         }

        public void myFilter()
        {
            FilterMode = true;

            filtered.Clear();
            //filtered = new ObservableCollection<Book>();
            string AuthorToBeFiltered = AuthorFilterText.Text;
            string TitleToBeFiltered = TitleFilterText.Text;
            string YearToBeFiltererd = YearsPool.Text;

            // string YearToBeFiltered = YearsPool.Text;


            if (AuthorToBeFiltered.Length == 0 && TitleToBeFiltered.Length == 0
                                                        && YearToBeFiltererd.Length == 0)
            {
                Debug.WriteLine(AuthorToBeFiltered);
                Debug.WriteLine(TitleToBeFiltered);
                Debug.WriteLine(YearToBeFiltererd);
                return;
            }
            
            foreach (Book book in books)
            {
                if (book.Author == AuthorToBeFiltered && book.Title == TitleToBeFiltered && book.Year == YearToBeFiltererd)
                {
                    Debug.WriteLine("Absolute Matching");
                    filtered.Add(book);
                    break;

                }
                //one of parameters matches exactly
                else if (book.Author.Length != 0 && book.Author == AuthorToBeFiltered)

                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    filtered.Add(book);
                    continue;
                }
                else if (book.Title.Length != 0 && book.Title == TitleToBeFiltered)
                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    filtered.Add(book);
                    continue;
                }
                else if (book.Year.Length != 0 && book.Year == YearToBeFiltererd)
                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    filtered.Add(book);
                    continue;
                }


                //if a string we are trying to filter contains name of some book from the library
                else if (book.Author.Length != 0 && AuthorToBeFiltered.Contains(book.Author))

                {
                    Debug.WriteLine("Some item in library has parameter a part of a name");
                    filtered.Add(book);
                    continue;
                }
                else if (book.Title.Length != 0 && TitleToBeFiltered.Contains(book.Title))
                {
                    Debug.WriteLine("Some item in library has parameter a part of a name");
                    filtered.Add(book);
                    continue;
                }

            }

            foreach (Book book in books)
            {
                if (book.Author.Contains(AuthorToBeFiltered) && AuthorToBeFiltered.Length != 0 && !filtered.Contains(book))
                {
                    Debug.WriteLine(AuthorToBeFiltered);
                    Debug.WriteLine("Parameters name contains some item from library");
                    filtered.Add(book);

                }
                else if (book.Title.Contains(TitleToBeFiltered) && TitleToBeFiltered.Length != 0 && !filtered.Contains(book))
                {
                    Debug.WriteLine(AuthorToBeFiltered);
                    Debug.WriteLine("Parameters name contains some item from library");
                    filtered.Add(book);

                }

            }
            ListOfBooks.ItemsSource = filtered;
            ListOfBooks.Items.Refresh();
            }

            public void ClearButtonClicked(object sender, EventArgs e)
            {
                ListOfBooks.ItemsSource = books;
            AuthorFilterText.Text = string.Empty;
            TitleFilterText.Text = string.Empty;
            YearsPool.Text = string.Empty;
            }

        }
 }
